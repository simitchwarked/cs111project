    if(count==10)
    {
        System.out.println("Escaping the clothing store you make your way to the autoshop, opening the door. A loud ringing noise is heard as a bell at the top of the door jingles, causing a walker to stumble out from its position behind the shelf. You easily take it down but now you’re concerned. There could be more walkers in the building…");
        System.out.println("1: I can’t take the risk. I know the walkers outside are alerted but if I let my guard down I could get killed. I should search the store for walkers before grabbing anything."); (1)
        System.out.println("2: I need to hurry, the walkers are really riled up and could detect me at any moment. Finding things and getting out is my top priority."); (5)
        System.out.println("3: I give a few whistles to see if anything comes out. If nothing does then I’ll hurry up and grab what I can."); (1)
        choice = scan.nextInt();
        if(choice==1)
        {
            points = points +1;
        }
        if(choice==2)
        {
            points = points +5;
        }
        if(choice==3)
        {
            points = points +1;

        }
    }


    if(count==11 && choice==1)
    {
        System.out.println("You search the store for walkers but don’t find anymore. Confident that it’s safe you grab some spark plugs and various mechanical tools and put them into your bag before heading towards the door.");
        System.out.println("You’ve successfully scavenged all there is in this part of town and night is falling.");
    }
    if(count==11 && choice==2)
    {
        System.out.println("You hurriedly grab some spark plugs and tools. However, in your haste you knock a tool box off the shelf and it comes crashing to the floor. As walkers start to trickle in you rush for the back door, escaping successfully.");
    }
    if(count==11 && choice==3)
    {
        System.out.println("You give a few soft whistles and, when nothing happens, you proceed to search the store. You grab some spark plugs and various other pieces of equipment before heading to the exit, confident that nothing else can be found here.");
    }

