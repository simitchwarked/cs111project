
import java.util.*;
public class TextCall
{
    private String String1, String2, String4;


    public TextCall(String S1, String S2, String S4)
    {
        String1 = S1;
        String2 = S2;
        String4 = S4;
    }

    public String getString1()
    {
        return "We have a situation.";
    }
    public String getString2()
    {
        return "We shave another problem";

    }
    public String getString4()
    {
        String a = "The Police station is along main street, right beside the court house. Across the street are a variety of local shops and businesses. Clothing stores, general stores, even a small market. The area looks pretty untouched and you should be able to find plenty of food and weapons here. A few walkers are on the road but they're no trouble.";
        String c = "Beginning your search you decide to first look through the Police Station. Entering you notice that the power is off. What do you do:";
        String e = "A. I pull out my flashlight. Stumbling around in the dark will do me no good, even though the light may attract walkers (1)";
        String g= "B. It isn't so dark and the windows are up so there's enough light to see where I’m walking. I don't want to risk waking up a large group of walkers, after all (2)";
        return a + "\n" + "\n" + c + "\n" + "\n" + e + "\n" + "\n" + g;
    }
}
