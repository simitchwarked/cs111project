


import java.util.Scanner;

public class WalkingDeadScenes
{

    public static void main (String args[])
    {
        //Create instance variables
        int count;
        int choice=0;
        int decision=0;


        Scanner scan = new Scanner(System.in);
        count=0;
        int points = 0;
        while(count<50)
        {
            count++;
            if(count==1)
            {
                System.out.println("**********************************************************************************************************");
                System.out.println("The Police station is along main street, right beside the court house. Across the street are a variety of local shops and businesses. Clothing stores, general stores, even a small market. The area looks pretty untouched and you should be able to find plenty of food and weapons here. A few walkers are on the road but they're no trouble.");
                System.out.println("Beginning your search you decide to first look through the Police Station. Entering you notice that the power is off. What do you do:");
                System.out.println("1) I pull out my flashlight. Stumbling around in the dark will do me no good, even though the light may attract walkers");
                System.out.println("2) It isn't so dark and the windows are up so there's enough light to see where I’m walking. I don't want to risk waking up a large group of walkers, after all");
                System.out.println("**********************************************************************************************************");
                choice = scan.nextInt();
                if(choice==1)
                {
                    points = points + 1;
                }
                if(choice==2)
                {
                    points = points + 2;
                }
            }
            if(count==2)
            {
                System.out.println("**********************************************************************************************************");

                System.out.println("Making your way through the station, you decide there are three places you can check first. The Armory, the administration or the evidence room. Despite the fact that there are without a doubt weapons in the armory, you can easily hear the shuffling of walkers behind the large, padlocked door. There doesn't seem to be anyone in the office block and the evidence room is at the end of the long hallway, also padlocked. What do you do:");
                System.out.println("1) Decide to break the lock on the armory door and get ready for a fight");
                System.out.println("2) Decide to check the office block. It's the safest course of action, though probably the least rewarding");
                System.out.println("3) Decide to break the lock on the evidence room. It doesn't sound like there is anything behind the doors, and I can probably find something of worth");
                System.out.println("**********************************************************************************************************");
                choice = scan.nextInt();
                if(choice==1);
                {
                    points = points + 4;
                }
                if(choice==2)
                {
                    points = points + 1;
                }
                if(choice==3)
                {
                    points = points + 3;
                }
            }
            if(count==3 && choice==1)
            {
                System.out.println("**********************************************************************************************************");
                System.out.println("You break the lock off the doors and manage to take down two walker police officers behind the door. Still, you notice that their weapons are still on their belts which is quite a find! Snagging the two handguns you rummage through the armory finding a few extra rounds to use. However, any other walkers in the building would have been alerted by the sound of the scuffle. What do you do:");
                System.out.println("1) Continue searching the Police Station. I have a feeling that you can still find more things... besides I have plenty of ammo now");
                System.out.println("2) Decide that I have enough weapons now. Food is the next biggest priority so I should head to the Grocery Store");
                System.out.println("3) I don't need any more weapons. I should look at the General stores to see if I can find some thicker Clothing and medicine as winter approaches.");
                System.out.println("**********************************************************************************************************");
                decision = scan.nextInt();
                if(decision==1);
                {
                    points = points + 5;
                }
                if(decision==2)
                {
                    points = points + 1;
                }
                if(decision==3)
                {
                    points = points + 1;
                }
            }
            if(count==3 && choice==2)
            {
                System.out.println("**********************************************************************************************************");
                System.out.println("You rummage through the offices, scanning for anything of import. While opening one of the lockers a few objects fall out, crashing to the floor. Inside the locker is a few extra rounds of ammo and some granola bars, but nothing else. Any walkers inside the building would have heard the noise though. You:");
                System.out.println("1) Continue searching the Police Station. I have a feeling that I can still find more things... besides I have plenty of ammo now");
                System.out.println("2) Decide that it isn’t worth the risk. My group needs food and I have plenty of ammo now. Time to check the grocery store");
                System.out.println("3) I don’t need the weapons anyway. The ammo I found is enough to ensure I won’t run out any time soon. With winter on the way I should try to bring back thicker clothing for my camp, maybe some medicine too. The General stores should be my next stop.");
                System.out.println("**********************************************************************************************************");
                decision = scan.nextInt();
                if(decision==1);
                {
                    points = points + 5;
                }
                if(decision==2)
                {
                    points = points + 1;
                }
                if(decision==3)
                {
                     System.out.print("While dodging through alleys you feel a walker grab hold of your ankle. You look down just in time to see it take a bite. It's game over for you.");
                    break;
                }
            }
            if(count==3 && choice==3)
            {
                System.out.println("**********************************************************************************************************");
                System.out.println("You break the lock and enter the room, only to be ambushed by a walker. You are able to take it down, thankfully, but only after a loud scuffle, alerting any other walkers in the station. Quickly scanning the shelves you don’t find anything except a few files and some articles of clothing. The room is a bust, do you:");
                System.out.println("1) Continue searching the Police Station. You have a feeling that you can still find more things...");
                System.out.println("2) The station is a bust, time to move on before you attract more attention. Gathering food should be your next priority, time to search he grocery store");
                System.out.println("3) There’s no time to waste, you should head over to the general stores. Its a ways away, but it's worth the risk. There you should be able to find some warmer clothes and possibly even some medicine for the winter.");
                System.out.println("**********************************************************************************************************");
                decision = scan.nextInt();
                if(decision==1);
                {
                    points = points + 5;
                }
                if(decision==2)
                {
                    points = points + 1;
                }
                if(decision==3)
                {
                    System.out.print("While dodging through alleys you feel a walker grab hold of your ankle. You look down just in time to see it take a bite. It's game over for you.");
                    break;
                }
            }
            if(count==4 && decision == 1)
            {
                System.out.println("***************************");
                System.out.println("The Police station could still yield results. You head back towards the lobby only to find that a few walkers outside must have heard the loud noises. You are forced to flee out the backdoor in order to escape their hungry clutches.");
                System.out.println("1) Time to head to the grocery store. The station will be swarming with walkers for a while now, no point in waiting");
                choice = 1;
            }
            if(count == 4 && decision==2)
            {
                System.out.println("***************************");
                System.out.println("You cautiously make your way out of the station, noticing almost a dozen walkers wandering into the station to investigate the noises. The street is clear now and you rush across entering the small grocery store.");
                System.out.println("1) I should head to the produce. Vegetables and fruits will be key to keeping everyone healthy! ");
                System.out.println("2) I should head to the canned goods: Canned goods are easily portable and provide decent nutritional value. I should get some of those. ");
                System.out.println("3) I should go to the deli. Protein and dairy products will be key to keeping everyone’s strength up in the coming months. ");
                choice = scan.nextInt();
                if(choice==1);
                {
                    points = points +4;
                }
                if(choice==2);
                {
                    points = points +1;
                }
                if(choice==3);
                {
                    points = points +2;
                }
            }
            if(count == 4 && decision==3)
            {
                System.out.println("***************************");
                System.out.println("You cautiously make your way out of the station, noticing almost a dozen walkers wandering into the station to investigate the noises. The street is clear now and you notice three stores that you should check out. Where will you search first?");
                System.out.println("1) The Pharmacy. It looks pretty damaged but I can probably still find some generic medicine while I’m there ");
                System.out.println("2) The clothing store. It doesn’t seem to have really been touched so there’s a good chance I can find some well-made clothes.");
                System.out.println("3) The Auto-Shop: With winter coming, making sure that we have the tools to fix our cars and trucks will be extremely important.");
                choice = scan.nextInt();
                if(choice==1);
                {
                    points = points +2;
                }
                if(choice==2);
                {
                    points = points +1;
                }
                if(choice==3);
                {
                    points = points +3;
                }
            }
            if(count == 5 && choice == 1)
            {
                System.out.println("***************************");
                System.out.println("You make your way towards the produce isle but, sadly, most of the food is gone. What little remains is either spoiled or rotten. However, there are some bags of carrots on the wall. You take them after ensuring they are still edible. You find some dried fruit as well that you stuff into your pack. Now you: ");
                System.out.println("1) Head over to canned goods ");
                System.out.println("2) Head over to the deli");
                decision = scan.nextInt();
                if(decision==1)
                {
                    points = points +1;
                }
                if(decision==2)
                {
                    points = points +2;
                }
            }
            if(count==5 && choice==2 || count==5 && choice==1)
            {
                System.out.println("*************************");
                System.out.println("You head to the canned goods isle and begin grabbing what little is left. As you round the corner, however, you find two walkers stumbling towards you.");
                System.out.println("1)I pull out my gun and fire two shots, taking down both walkers. If I kill them I can resume my search.");
                System.out.println("2)I pull out my knife, getting in close with the two walkers, hoping I can take down both of them. If I kill them I can keep scavenging.");
                System.out.println("3)I run. There’s no need to fight them. I’ve already got a good amount of food so it won’t be bad if I leave now. ");
                decision = scan.nextInt();
                if(decision==1)
                {
                    System.out.println("The two walkers drop to the ground, your expertly placed shots killing them. As you turn to leave however, the doors to the back of the store fly open, revealing a large group of hungry walkers heading towards you. You turn and make your way for the front door only to find more walkers closing in. Surrounded on all sides by walkers you realize that this is the end of you.");
                    break;
                }
                if(decision==2)
                {
                    points = points +5;
                    System.out.print("Well done, you manage to take them down no problem. You continue scavenging grabbing a few items before you notice more walkers have wandered in. You decide its time to leaves as staying would surely get you killed.");
                }
                if(decision==3)
                {
                    points = points +1;
                }
            }
            if(count==5 && choice==3 || count==5 && choice==2)
            {
                System.out.println("***************************");
                System.out.println("You make your way to the Deli, hopping the counter to go search the back. As you search various boxes you pull out some packaged ham that is still fresh and stuff it in your backpack. Before you can continue your search you hear the sound of cans being knocked to the ground as some walkers come shambling around the corner. They haven’t seen you yet, now is the time to act.");
                System.out.println("1) I head to the storage unit, ripping open the large metal door. I’ll be safe in here, after all the walkers won’t be able to open the heavy door.");
                System.out.println("2) I vault the counter and make my way to the exit. I have a good amount of food in my bag and there’s plenty of time to search the rest of the area.");
                System.out.println("3) I take out my knife and rush the walkers, taking the two of them down in the close quarters of the hallway.");

                decision = scan.nextInt();
                if(decision==1)
                {
                    System.out.println("As you close the door to the storage unit the lights flicker on, revealing a large group of former employees. The walkers notice you and charge. You pull out your gun, firing into the crowd but there are too many and you are overwhelmed");
                    break;
                }
                if(decision==2)
                {
                    points = points +1;
                }
                if(decision==3)
                {
                    points = points +5;
                    System.out.print("Well done, you manage to take them down no problem. You continue scavenging grabbing a few items before you notice more walkers have wandered in. You decide its time to leaves as staying would surely get you killed.");
                }
            }

            if(count==6 && decision==2 || count==6 && decision==3 )
            {
                System.out.println("***************************************************");
                System.out.println("Leaving the grocery store, you decide to make your way to the streets with the general stores. However, as they are far out of your way, you need to take a detour through some run down homes. Entering the first home quietly, you find signs that someone has been living here. Cans and clothes litter the floor a warm bowl of soup sits on the table. Before you can act the man, armed with a knife, sneaks up on you and demands your gear, threatening to kill you if you don’t comply.");
                System.out.println("1) I concede to his demands. It’s only supplies, they aren’t worth my life!");
                System.out.println("2) I try to persuade him by giving him some of my food. I can’t give up all of it though, there are people counting on me!");
                System.out.println("3) I can’t let this guy take my supplies. He’s got a knife but I have a gun… I don’t have any other choice, even though the shot will attract other walkers");

                choice = scan.nextInt();
                if(choice==1)
                {
                    points = points +5;
                }
                if(choice==2)
                {
                    System.out.println("The man is unimpressed by your attempts to make a compromise. With your arm outstretched with an offering of some food, he lunges at you and drives you to the ground");
                    break;
                }
                if(choice==3)
                {
                    points = points +1;
                }
            }

            if(count==7 && choice==1)
            {
                System.out.println("********************************************************************");
                System.out.println("You hand over the supplies and the man greedily takes your all your gear. He gives you back your pack and tells you to leave. In defeat you run out of his house heading towards the store, knowing that now you really need to succeed! You arrive at the nearest of the general stores, the pharmacy, and enter. The first thing you notice is the stench of dead bodies.");
                System.out.println("You find a few bottles of Tylenol and some ibuprofen, but not much else. There are probably some more pills in the back, but there’s a huge chance that a lot of walkers are back there behind the closed doors.");
                System.out.println("1) I can’t be afraid of a few dead bodies, walkers or not. I came to get medicine and that’s what I’m going to do!");
                System.out.println("2) It is way too risky, better just settle with the generic stuff that’s still out here");

                decision = scan.nextInt();
                if(decision==1)
                {
                    points = points +2;
                }
                if(decision==2)
                {
                    points = points +1;
                }
            }

            if(count==7 && choice==3)
            {
                System.out.println("**********************************************************");
                System.out.println("Your draw your weapon in the blink of an eye. As the man charges in your direction you open fire, taking him down easily enough. The walkers outside are alerted too your presence and you sneak out the back window. You arrive at the street to find that there are three businesses that would be worth checking out. Making your way towards the nearest of the general stores, the pharmacy, you enter. The first thing you notice is the stench of dead bodies. You find a few bottles of Tylenol and some ibuprofen, but not much else. There are probably some more pills in the back, but there’s a huge chance that a lot of walkers are back there behind the closed doors.");
                System.out.println("1) I can’t be afraid of a few dead bodies, walkers or not. I came to get medicine and that’s what I’m going to do");
                System.out.println("2) It is way too risky, better just settle with the generic stuff that’s still out here");

                decision = scan.nextInt();
                if(decision==1)
                {
                    points = points +2;
                }
                if(decision==2)
                {
                    points = points +1;
                }
            }

            if(count==8 && decision==1)
            {
                System.out.println("***************************************************************");
                System.out.println("Heading towards the back you pull open the door only to find that, thankfully, the dead bodies aren’t moving. They must have already been put down. You step over them and find a few extra bottles of cold medicine and anti-inflammation medicine and make your way out of the pharmacy and head to the Clothing Store to search for winter gear.");
                System.out.println("You enter the clothing store. As you thought, it’s pretty untouched and a lot of clothes line the walls and display racks. The winter clothes section is over to the right, but there is also some camping equipment on the other side of the store.");
                System.out.println("1) We can sleep in our cars, staying warm while we are on the move is the most important. I’ll check the camping equipment later. With the all the walkers outside I need to move fast.");
                System.out.println("2) We can stay warm if we have tents and bedrolls. We’d even be able to make a camp in mountains where the walkers would be less likely to find us!");

                choice = scan.nextInt();
                if(choice==1)
                {
                    points = points +1;
                }
                if(choice==2)
                {
                    points = points +3;
                }
            }

            if(count==8 && decision==2)
            {
                System.out.println("***************************************************************");
                System.out.println("Deciding it isn’t worth the risk, you leave the pharmacy, content with what you have and head to the clothing store. You enter the clothing store. As you thought, it’s pretty untouched and a lot of clothes line the walls and display racks. The winter clothes section is over to the right, but there is also some camping equipment on the other side of the store.");
                System.out.println("1) We can sleep in our cars, staying warm while we are on the move is the most important. I’ll check the camping equipment later. With the all the walkers outside I need to move fast.");
                System.out.println("2) We can stay warm if we have tents and bedrolls. We’d even be able to make a camp in mountains where the walkers would be less likely to find us!");

                choice = scan.nextInt();
                if(choice==1)
                {
                    points = points +1;
                }
                if(choice==2)
                {
                    points = points +3;
                }
            }

            if(count==9 && choice==1)
            {
                System.out.println("***************************");
                System.out.println("You make your way to the clothing isle. There are a few bags here that you fill with some thermal shirts and some thick socks. In your haste to grab on of the thicker jackets from the top you knock over a lamp which shatters on the ground. You hear the door open as walkers pour in.");
                System.out.println("1: I have to get out of here! No time to grab anything else, what I have is what we’ll have to work with!");
                System.out.println("2: Continue to reach for the jacket to grab it and then evacuate. That jacket will come in handy, no point in leaving it behind when I’m so close!");
                decision = scan.nextInt();

                if(decision==1)
                {
                    points = points +1;
                }
                if(decision==2)
                {
                    System.out.println("As you grab the bottom of the jacket you find it’s hard to pull off its hangar. Yanking hard you lose your balance and fall to the ground, only to look up at the face of hungry walkers. You have died");
                    break;
                }
            }

            if(count==9 && choice==2)
            {
                System.out.println("***************************");
                System.out.println("Making your way to the camping section you discover, to your dismay, that most of the stuff will be too big to fit in your pack. Still, you can grab a few tarps that can be used to make a shelter outside your groups RV. Grabbing them and stuffing them into your bag you wind up accidentally knocking a mannequin. You hear the door pushed open by walkers who heard the noise.");
                System.out.println("1: There is only a few of them so I should still try to sneak over to the clothing isle. That was the whole reason I came into the store after all.");
                System.out.println("2: I don’t care how little of them there are, I have to get out of here!");

                decision = scan.nextInt();
                if(decision==1)
                {
                    System.out.println("You make your way over to the clothing isle. To ensure you don’t walk into any walkers, you fail to notice the lamp on the table. Your pack knocks into it, causing it to hit the ground and shatter. You try to make a dash for the exit but a walker grabs you and pulls you back. You have died.");
                    break;
                }
                if(decision==2)
                {
                    points = points +1;
                }
            }

            if(count==10)
            {
                System.out.println("Escaping the clothing store you make your way to the autoshop, opening the door. A loud ringing noise is heard as a bell at the top of the door jingles, causing a walker to stumble out from its position behind the shelf. You easily take it down but now you’re concerned. There could be more walkers in the building…");
                System.out.println("1: I can’t take the risk. I know the walkers outside are alerted but if I let my guard down I could get killed. I should search the store for walkers before grabbing anything.");
                System.out.println("2: I need to hurry, the walkers are really riled up and could detect me at any moment. Finding things and getting out is my top priority.");
                System.out.println("3: I give a few whistles to see if anything comes out. If nothing does then I’ll hurry up and grab what I can.");
                choice = scan.nextInt();
            if(choice==1)
            {
                points = points +1;
            }
            if(choice==2)
            {
                points = points +5;
            }
            if(choice==3)
            {
                points = points +1;

            }
            }


            if(count==11 && choice==1)
            {
                System.out.println("You search the store for walkers but don’t find anymore. Confident that it’s safe you grab some spark plugs and various mechanical tools and put them into your bag before heading towards the door.");
                System.out.println("You’ve successfully scavenged all there is in this part of town and night is falling.");
            }
            if(count==11 && choice==2)
            {
                System.out.println("You hurriedly grab some spark plugs and tools. However, in your haste you knock a tool box off the shelf and it comes crashing to the floor. As walkers start to trickle in you rush for the back door, escaping successfully.");
            }
            if(count==11 && choice==3)
            {
                System.out.println("You give a few soft whistles and, when nothing happens, you proceed to search the store. You grab some spark plugs and various other pieces of equipment before heading to the exit, confident that nothing else can be found here.");
            }

            if(count==12)
            {
                System.out.println("You look to the sky and realize it’s getting darker. Proud of yourself for braving the terrors of the walker infested town for supplies you make your way towards the abandoned lot where your car is waiting. Climbing into the driver’s seat you turn the key and head out of the town, back to a camp that is eagerly awaiting you and the items you have gathered.");
            }

            if(points > 25)
            {
                System.out.println("As you make your way about the task at hand your mind begins to wander to the people back at camp. You wonder what they’re doing right now, if they’re all sitting around their fires eagerly awaiting your return.  You wish you were there but, at the same time, are glad you’re able to pull your own weight. However, a hand on your shoulder brings you out of your wandering thoughts and you turn only to come face to face with a walker, the last thing you will ever see. Your careless mistakes along the way have paved the way for your death.");
                break;
            }

            }

    }
}

