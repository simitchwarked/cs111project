if(count==4 && choice == 1)
    System.out.println("***************************");

    System.out.println("The Police station could still yield results. You head back towards the lobby only to find that a few walkers outside must have heard the loud noises. You are forced to flee out the backdoor in order to escape their hungry clutches.");
    System.out.println("1) Time to head to the grocery store. The station will be swarming with walkers for a while now, no point in waiting");
    System.out.println("2) Time to search all the general stores.");

    choice = scan.nextInt();
    if(choice==1);
    {
        points = points +1
    }
    if(choice==2);
    {
        points = points +1
    }
if(count == 4 && choice==2)
    System.out.println("***************************");

    System.out.println("You cautiously make your way out of the station, noticing almost a dozen walkers wandering into the station to investigate the noises. The street is clear now and you rush across entering the small grocery store.")
    System.out.println("1) I should head to the produce. Vegetables and fruits will be key to keeping everyone healthy! ")
    System.out.println("2) I should head to the canned goods: Canned goods are easily portable and provide decent nutritional value. I should get some of those. ")
    System.out.println("3) I should go to the deli. Protein and dairy products will be key to keeping everyone’s strength up in the coming months. ")

    choice = scan.nextInt();
    if(choice==1);
    {
        points = points +4
    }
        if(choice==2);
    {
        points = points +1
    }
        if(choice==3);
    {
        points = points +2
    }

if(count == 4 && choice==3)
    System.out.println("***************************");
    System.out.println("You cautiously make your way out of the station, noticing almost a dozen walkers wandering into the station to investigate the noises. The street is clear now and you notice three stores that you should check out. Where will you search first?");
    System.out.println("1) The Pharmacy. It looks pretty damaged but I can probably still find some generic medicine while I’m there ");
    System.out.println("2) The clothing store. It doesn’t seem to have really been touched so there’s a good chance I can find some well-made clothes.");
    System.out.println("3) The Auto-Shop: With winter coming, making sure that we have the tools to fix our cars and trucks will be extremely important.");

    choice = scan.nextInt();
    if(choice==1);
    {
        points = points +2
    }
    if(choice==2);
    {
        points = points +1
    }
    if(choice==3);
    {
        points = points +3
    }
if(count == 5 && choice == 1)
    System.out.println("***************************");
    System.out.println("You make your way towards the produce isle but, sadly, most of the food is gone. What little remains is either spoiled or rotten. However, there are some bags of carrots on the wall. You take them after ensuring they are still edible. You find some dried fruit as well that you stuff into your pack. Now you: ");
    System.out.println("1) Head over to canned goods ");
    System.out.println("2) Head over to the deli");

    decision = scan.nextInt();
    if(decision==1)
        {
            points = points +1
        }
    if(decision==2)
        {
            points = points +2
        }
if(count==5 && choice==2 || decision==1)
    System.out.println("***************************");
    System.out.println("You head to the canned goods isle and begin grabbing what little is left. As you round the corner, however, you find two walkers stumbling towards you.");
    System.out.println("1)I pull out my gun and fire two shots, taking down both walkers. If I kill them I can resume my search.")
    System.out.println("2)I pull out my knife, getting in close with the two walkers, hoping I can take down both of them. If I kill them I can keep scavenging.");
    System.out.println("3)I run. There’s no need to fight them. I’ve already got a good amount of food so it won’t be bad if I leave now. ")

    decision = scan.nextInt();
    if(decision==1)
        {
            System.out.println("The two walkers drop to the ground, your expertly placed shots killing them. As you turn to leave however, the doors to the back of the store fly open, revealing a large group of hungry walkers heading towards you. You turn and make your way for the front door only to find more walkers closing in. Surrounded on all sides by walkers you realize that this is the end of you.");
            break;
        }
    if(decision==2)
        {
            points = points +5
        }
    if(decision==3)
        {
            points = points +1
        }
if(count==5 && choice==3 || count==5 decision==2)
    System.out.println("***************************");
    System.out.println("You make your way to the Deli, hopping the counter to go search the back. As you search various boxes you pull out some packaged ham that is still fresh and stuff it in your backpack. Before you can continue your search you hear the sound of cans being knocked to the ground as some walkers come shambling around the corner. They haven’t seen you yet, now is the time to act.");
    System.out.println("1) I head to the storage unit, ripping open the large metal door. I’ll be safe in here, after all the walkers won’t be able to open the heavy door.");
    System.out.println("2) I vault the counter and make my way to the exit. I have a good amount of food in my bag and there’s plenty of time to search the rest of the area.");
    System.out.println("3) I take out my knife and rush the walkers, taking the two of them down in the close quarters of the hallway.");

    decision = scan.nextInt();
    if(decision==1)
        {
            System.out.println("As you close the door to the storage unit the lights flicker on, revealing a large group of former employees. The walkers notice you and charge. You pull out your gun, firing into the crowd but there are too many and you are overwhelmed");
            break;
        }
    if(decision==2)
        {
            points = points +1
        }
    if(decision==3)
        {
            points = points +5
        }
if(count==6 && decision==2 || count==6 decision==3)
    System.out.println("***************************************************");
    System.out.println("Leaving the grocery store, you decide to make your way to the streets with the general stores. However, as they are far out of your way, you need to take a detour through some run down homes. Entering the first home quietly, you find signs that someone has been living here. Cans and clothes litter the floor a warm bowl of soup sits on the table. Before you can act the man, armed with a knife, sneaks up on you and demands your gear, threatening to kill you if you don’t comply.");
    System.out.println("1) I concede to his demands. It’s only supplies, they aren’t worth my life!");
    System.out.println("2) I try to persuade him by giving him some of my food. I can’t give up all of it though, there are people counting on me!");
    System.out.println("3) I can’t let this guy take my supplies. He’s got a knife but I have a gun… I don’t have any other choice, even though the shot will attract other walkers");

    choice = scan.nextInt();
    if(choice==1)
        {
            points = points +5
        }
    if(choice==2)
        {
            System.out.println("The man is unimpressed by your attempts to make a compromise. With your arm outstretched with an offering of some food, he lunges at you and drives you to the ground");
            break;
        }
    if(choice==3)
        {
            points = points +1
        }

if(count==7 && choice==1)
    System.out.println("********************************************************************");
    System.out.println("You hand over the supplies and the man greedily takes your all your gear. He gives you back your pack and tells you to leave. In defeat you run out of his house heading towards the store, knowing that now you really need to succeed! You arrive at the nearest of the general stores, the pharmacy, and enter. The first thing you notice is the stench of dead bodies.");
    System.out.println("You find a few bottles of Tylenol and some ibuprofen, but not much else. There are probably some more pills in the back, but there’s a huge chance that a lot of walkers are back there behind the closed doors.");
    System.out.println("1) I can’t be afraid of a few dead bodies, walkers or not. I came to get medicine and that’s what I’m going to do!");
    System.out.println("2) It is way too risky, better just settle with the generic stuff that’s still out here");

    decision = scan.nextInt();
    if(decision==1)
        {
            points = points +2
        }
    if(decision==2)
        {
            points = points +1
        }
if(count==7 && choice==3)
    System.out.println("**********************************************************");
    System.out.println("Your draw your weapon in the blink of an eye. As the man charges in your direction you open fire, taking him down easily enough. The walkers outside are alerted too your presence and you sneak out the back window. You arrive at the street to find that there are three businesses that would be worth checking out. Making your way towards the nearest of the general stores, the pharmacy, you enter. The first thing you notice is the stench of dead bodies. You find a few bottles of Tylenol and some ibuprofen, but not much else. There are probably some more pills in the back, but there’s a huge chance that a lot of walkers are back there behind the closed doors.");
    System.out.println("1) I can’t be afraid of a few dead bodies, walkers or not. I came to get medicine and that’s what I’m going to do");
    System.out.println("2) It is way too risky, better just settle with the generic stuff that’s still out here");

    decision = scan.nextInt();
    if(decision==1)
        {
            points = points +2
        }
    if(decision==2)
        {
            points = points +1
        }
if(count==8 && decision==1)
    System.out.println("***************************************************************");
    System.out.println("Heading towards the back you pull open the door only to find that, thankfully, the dead bodies aren’t moving. They must have already been put down. You step over them and find a few extra bottles of cold medicine and anti-inflammation medicine and make your way out of the pharmacy and head to the Clothing Store to search for winter gear.");
    System.out.println("You enter the clothing store. As you thought, it’s pretty untouched and a lot of clothes line the walls and display racks. The winter clothes section is over to the right, but there is also some camping equipment on the other side of the store.");
    System.out.println("1) We can sleep in our cars, staying warm while we are on the move is the most important. I’ll check the camping equipment later. With the all the walkers outside I need to move fast.");
    System.out.println("2) We can stay warm if we have tents and bedrolls. We’d even be able to make a camp in mountains where the walkers would be less likely to find us!");

    choice = scan.nextInt();
    if(choice==1)
        {
            points = points +1
        }
    if(choice==2)
        {
            points = points +3
        }
if(count==8 && decision==2)
    System.out.println("***************************************************************");
    System.out.println("Deciding it isn’t worth the risk, you leave the pharmacy, content with what you have and head to the clothing store. You enter the clothing store. As you thought, it’s pretty untouched and a lot of clothes line the walls and display racks. The winter clothes section is over to the right, but there is also some camping equipment on the other side of the store.");
    System.out.println("1) We can sleep in our cars, staying warm while we are on the move is the most important. I’ll check the camping equipment later. With the all the walkers outside I need to move fast.");
    System.out.println("2) We can stay warm if we have tents and bedrolls. We’d even be able to make a camp in mountains where the walkers would be less likely to find us!");

    choice = scan.nextInt();
    if(choice==1)
        {
            points = points +1
        }
    if(choice==2)
        {
            points = points +3
        }


























