public class WalkingDeadIntro
{
    public static void Intro()
    {
        System.out.println("");
        System.out.println("You are in a post apocalyptic world brought on by the outbreak of a strange illness. This illness turns those infected with it into zombies, which we like to call “walkers”. You can become a walker one of two ways, through death or by being bitten by a walker, so it is important to always watch your back.");
        System.out.println("");
        System.out.println("You have managed to survive this long and have a pretty solid group of people by your side. The past couple of weeks had been good for  your group until a herd of walkers attacked your camp. Most of your people survived but now your food and supplies are severely depleted. Someone needs to go try to find supplies before its too late. Luckily for you, you happen to be the groups best scout so this mission is all yours. It would be nice to bring someone along with you but its too risky, you move better on your own. ");
        System.out.println("");
        System.out.println("You must make the right decisions to get yourself out alive. Any wrong move and you will be starring death in the face. Your decisions start as you enter town starting with the police station. Good Luck!");
        System.out.println("");
    }
}
